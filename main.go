package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"os"

	"testProject/routers"
)

func main()  {
	env := godotenv.Load()
	if env != nil {
		fmt.Println(env)
	}

	r := routers.SetupRouter()

	port := os.Getenv("port")

	if port == "" {
		port = "8080"
	}

	r.Run(":" + port)
}
