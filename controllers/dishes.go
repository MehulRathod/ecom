package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	u "testProject/apiHelpers"
	service "testProject/services"
)

func CreateDishes(c *gin.Context)  {
	var dishesService service.DishesService

	err := json.NewDecoder(c.Request.Body).Decode(&dishesService.Dishes)
	if err != nil {
		u.Respond(c.Writer, u.Message(1, "invalid request"))
		return
	}

	response := dishesService.CreateDishes()
	u.Respond(c.Writer, response)
	return
}
