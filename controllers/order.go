package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	u "testProject/apiHelpers"
	service "testProject/services"
)

func CreateOrder(c *gin.Context)  {
	var order service.OrderService

	userInfo := c.MustGet("userData")
	data := userInfo.(map[string]interface{})
	var ID = uint(data["ID"].(float64))

	var v interface{}
	err := json.NewDecoder(c.Request.Body).Decode(&v)
	if err != nil {
		u.Respond(c.Writer, u.Message(1, "invalid request"))
	}
	m := v.(map[string]interface{})
	fmt.Println(m)
	orderId := m["order_id"].(float64)
	itemId := m["dishes_id"].(float64)
	qty := m["quantity"].(float64)
	restaurantId := m["restaurant_id"].(float64)

	if itemId == 0 || qty == 0 {
		u.Respond(c.Writer, u.Message(1, "invalid request"))
	}
	//else if orderId == "" || restaurantId == "" {
	//	u.Respond(c.Writer, u.Message(1, "invalid request"))
	//}

	if orderId == 0 {
		orderId = order.CreateOrder(restaurantId, ID)
	}
	order.AddItemToOrder(orderId, itemId, qty)

	//response := order.CreateOrder()
	//u.Respond(c.Writer, response)
	return
}

/*userInfo := c.MustGet("userData")
data := userInfo.(map[string]interface{})
var ID = uint(data["ID"].(float64))*/