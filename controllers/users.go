package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	helpers "testProject/apiHelpers"
	u "testProject/apiHelpers"
	"testProject/resources"
	service "testProject/services"
)

func Register(c *gin.Context) {
	var userService service.UserService
	var user resources.SignUpRequest

	if err := c.MustBindWith(&user, binding.FormMultipart); err != nil {
		u.Respond(c.Writer, u.Message(1, "Invalid Request"))
		return
	}
	fmt.Println("y invalid", user)
	fmt.Println("y invalid", userService)
	ImageName, err := helpers.ImageUpload(c, "images/users", "user")
	if err {
		u.Respond(c.Writer, u.Message(1, "Upload valid file"))
	}
	user.ImageName = ImageName

	response := userService.RegisterUser(user)
	u.Respond(c.Writer, response)
	return
}

func Login(c *gin.Context) {
	var v interface{}
	err := json.NewDecoder(c.Request.Body).Decode(&v)
	if err != nil {
		u.Respond(c.Writer, u.Message(1, "invalid request"))
		return
	}
	m := v.(map[string]interface{})
	fmt.Println(" expecting values here >>>", m)
	email_mobile := m["email_mobile"].(string)
	password := m["password"].(string)
	fmt.Println(" expecting values email_mobile here >>>", email_mobile)
	fmt.Println(" expecting values password here >>>", password)

	response := service.UserLogin(email_mobile, password)
	u.Respond(c.Writer, response)
}

func ListOfUsers(c *gin.Context) {
	response := service.RegisteredUserList(c)
	u.Respond(c.Writer, response)
}

func UserDetails(c *gin.Context) {
	var userService service.UserService
	response := userService.UserDetails(c)
	u.Respond(c.Writer, response)
}

func UpdateUserDetails(c *gin.Context) {
	userAuth := helpers.AuthUser(c)
	UId := uint(userAuth["ID"].(float64))

	var userService service.UserService
	var user resources.EditProfileRequest

	if err := c.MustBindWith(&user, binding.FormMultipart); err != nil {
		u.Respond(c.Writer, u.Message(1, "Invalid Request"))
		return
	}

	ImageName, err := helpers.ImageUpload(c, "images/users", "user")
	if err {
		u.Respond(c.Writer, u.Message(1, "Upload valid file"))
		return
	}
	user.ImageName = ImageName
	user.ID = UId
	response := userService.UpdateDetails(user)
	u.Respond(c.Writer, response)
	return
}
