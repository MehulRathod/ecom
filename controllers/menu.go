package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	u "testProject/apiHelpers"
	"testProject/models"
	"testProject/services"
)

func CreateMenu(c *gin.Context)  {
	var menu services.MenuService

	err := json.NewDecoder(c.Request.Body).Decode(&menu.Menu)
	if err != nil {
		u.Respond(c.Writer, u.Message(1, "invalid request"))
		return
	}

	response := menu.CreateMenu(c)
	u.Respond(c.Writer, response)
	return
}

func EditMenu(c *gin.Context)  {
	var menu services.MenuService

	err := json.NewDecoder(c.Request.Body).Decode(&menu.Menu)
	if err != nil {
		u.Respond(c.Writer, u.Message(1, "invalid request"))
		return
	}

	response := menu.EditMenu(c)
	u.Respond(c.Writer, response)
	return
}

func GetAllMenu(c *gin.Context)  {
	var menu []models.Menu
	var results = models.GetRedisDB().Get(c, u.MENU).Val()
	json.Unmarshal([]byte(results), &menu)
	if len(menu) == 0 {
		services.StoreMenuInRedis(c)
	}
	u.Respond(c.Writer, menu)
	return
}
