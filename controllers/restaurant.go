package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	u "testProject/apiHelpers"
	"testProject/services"
)

func Restaurant(c *gin.Context)  {
	var restaurant services.RestaurantService

	err := json.NewDecoder(c.Request.Body).Decode(&restaurant.Restaurant)
	if err != nil {
		u.Respond(c.Writer, u.Message(1, "invalid request"))
		return
	}

	response := restaurant.CreateRestaurant()
	u.Respond(c.Writer, response)
	return
}
