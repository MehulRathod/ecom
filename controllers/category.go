package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	u "testProject/apiHelpers"
	"testProject/models"
	"testProject/services"
)

func CreateCategory(c *gin.Context)  {
	var category services.CategoryService

	err := json.NewDecoder(c.Request.Body).Decode(&category.Category)
	if err != nil {
		u.Respond(c.Writer, u.Message(1, "invalid request"))
		return
	}

	response := category.CreateCategory(c)
	u.Respond(c.Writer, response)
	return
}

func EditCategory(c *gin.Context)  {
	var category services.CategoryService

	err := json.NewDecoder(c.Request.Body).Decode(&category.Category)
	if err != nil {
		u.Respond(c.Writer, u.Message(1, "invalid request"))
		return
	}

	response := category.EditCategory(c)
	u.Respond(c.Writer, response)
	return
}

func GetAllCategory(c *gin.Context)  {
	var categories []models.Category
	var results = models.GetRedisDB().Get(c, u.CATEGORIES).Val()
	json.Unmarshal([]byte(results), &categories)
	if len(categories) == 0 {
		services.StoreCategoriesInRedis(c)
	}
	u.Respond(c.Writer, categories)
	return
}
