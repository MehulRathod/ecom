package resources

type OrderItemResponse struct {
	OrderID   uint    `json:"order_id"`
	DishesID  uint    `json:"dishes_id"`
	Price     float64 `json:"price"`
	Quantity  int64   `json:"quantity"`
	BasePrice float64 `json:"base_price"`
}
