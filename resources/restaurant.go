package resources

type RestaurantResponse struct {
	RestaurantID uint   `json:"restaurant_id"`
	Name         string `json:"name"`
	Address      string `json:"address"`
	Mobile       string `json:"mobile"`
}
