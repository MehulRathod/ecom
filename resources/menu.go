package resources

type MenuResponse struct {
	RestaurantID uint   `json:"restaurant_id"`
	MenuName     string `json:"menu_name"`
	StatusId     string `json:"status_id"`
	Description  string `json:"description"`
}

type MenuEditResponse struct {
	RestaurantID uint   `json:"restaurant_id"`
	MenuName     string `json:"menu_name"`
	StatusId     string `json:"status_id"`
	Description  string `json:"description"`
}

type GetAllMenuResponse struct {
	RestaurantID uint   `json:"restaurant_id"`
	MenuName     string `json:"menu_name"`
	StatusId     string `json:"status_id"`
	Description  string `json:"description"`
}
