package resources

import (
	"mime/multipart"
	"time"
)

type SignUpRequest struct {
	ID        uint
	Name      string                `form:"name" binding:"required"`
	Email     string                `form:"email" binding:"required,email"`
	Image     *multipart.FileHeader `form:"image" binding:"required"`
	ImageName string
	Password  string    `form:"password" binding:"required"`
	Mobile    string    `form:"mobile" binding:"required"`
	CreatedAt time.Time `form:"created_at" sql:"DEFAULT:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `form:"updated_at" sql:"DEFAULT:CURRENT_TIMESTAMP"`
}

type UserSignUpResponse struct {
	Name   string `gorm:"type:varchar(50)" form:"name"`
	Email  string `gorm:"type:varchar(50)" form:"email" validate:"required,email"`
	Mobile string `gorm:"type:varchar(15)" form:"mobile"`
	Image  string `form:"image,omitempty"`
}

type PassDataToTokenUser struct {
	ID uint
}

type LoginResponse struct {
	ID     uint   `gorm:"primary_key" form:"id"`
	Name   string `gorm:"type:varchar(50)" form:"name,omitempty"`
	Email  string `gorm:"type:varchar(50)" form:"email,omitempty" validate:"required,email"`
	Mobile string `gorm:"type:varchar(15)" form:"mobile"`
	Image  string `form:"image,omitempty"`
	Token  string `form:"token"`
}

type UserLoginResponse struct {
	ID     uint   `gorm:"primary_key" form:"id"`
	Name   string `gorm:"type:varchar(50)" form:"name,omitempty"`
	Email  string `gorm:"type:varchar(50)" form:"email,omitempty" validate:"required,email"`
	Mobile string `gorm:"type:varchar(15)" form:"mobile"`
	Image  string `form:"image,omitempty"`
}

type EditProfileRequest struct {
	ID        uint
	Name      string                `form:"name" binding:"required"`
	Email     string                `form:"email" binding:"required,email"`
	Image     *multipart.FileHeader `form:"image" binding:"required"`
	ImageName string
	Mobile    string `form:"mobile" binding:"required"`
}
