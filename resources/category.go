package resources

type CategoryResponse struct {
	MenuID       uint   `json:"menu_id"`
	CategoryName string `json:"category_name"`
	StatusId     string `json:"status_id"`
	Description  string `json:"description"`
}

type CategoryEditResponse struct {
	MenuID       uint   `json:"menu_id"`
	CategoryName string `json:"category_name"`
	StatusId     string `json:"status_id"`
	Description  string `json:"description"`
}
