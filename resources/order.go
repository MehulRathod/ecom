package resources

type OrderResponse struct {
	RestaurantID uint    `json:"restaurant_id"`
	DishesID     uint    `json:"dishes_id"`
	UserID       uint    `json:"user_id,omitempty"`
	OrderPrice   float64 `json:"order_price"`
}
