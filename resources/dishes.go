package resources

type DishesResponse struct {
	CategoryID uint    `json:"category_id"`
	DishName   string  `json:"dish_name"`
	Price      float64 `json:"price"`
	Quantity   int     `json:"quantity"`
}
