package redis

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"testProject/models"
	"time"
)

var redisClient *redis.Client



func init(){
	redisClient = models.GetRedisDB()
}

func Get(ctx context.Context, key string)  {
	val, err := redisClient.Get(ctx, "key").Result()
	switch {
	case err == redis.Nil:
		fmt.Println("key does not exist")
	case err != nil:
		fmt.Println("Get failed", err)
	case val == "":
		fmt.Println("value is empty")
	}
}

func Set(ctx context.Context, key string, value string, expiry time.Duration) error  {
	err := redisClient.Set(ctx, "key", "value", 0).Err()
	if err != nil {
		panic(err)
	}
	return nil
}