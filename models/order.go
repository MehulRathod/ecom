package models

type Order struct {
	Model
	RestaurantID uint    `type:"int(11)" sql:"index" json:"restaurant_id"`
	UserID       uint    `type:"int(11)" sql:"index" json:"user_id"`
	OrderPrice   float64 `json:"order_price"`
	StatusId     string `gorm:"type:enum('active','inactive')" sql:"index" json:"status_id"`

	//Restaurant   Restaurant `gorm:"foreignkey:RestaurantID"`
	//User         User       `gorm:"foreignkey:UserID"`
}

func (u *Order) TableName() string {
	return "orders"
}
