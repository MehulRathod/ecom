package models

type Category struct {
	Model
	MenuID       uint   `type:"int(11)" sql:"index" json:"menu_id"`
	CategoryName string `gorm:"type:varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci" sql:"index" json:"category_name"`
	StatusId     string `gorm:"type:enum('active','inactive','deleted')" sql:"index" json:"status_id"`
	Description  string `gorm:"type:text" json:"description"`
	//Menu         Menu   `gorm:"foreignkey:MenuID"`
}

func (u *Category) TableName() string {
	return "category"
}
