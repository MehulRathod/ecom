package models

type User struct {
	Model
	Name     string `gorm:"type:varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci" json:"name"`
	Email    string `gorm:"type:varchar(50)" sql:"index" json:"email" validate:"required,email"`
	Image    string `gorm:"type:varchar(255)" json:"image"`
	Password string `gorm:"type:varchar(100)" sql:"index" json:"password" validate:"required"`
	Mobile   string `gorm:"type:varchar(15)" sql:"index" json:"mobile" validate:"numeric"`
}

func (u *User) TableName() string {
	return "users"
}
