package models

type Dishes struct {
	Model
	CategoryID uint     `type:"int(11)" sql:"index" json:"category_id"`
	DishName   string   `gorm:"type:varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci" sql:"index" json:"dish_name"`
	Price      float64  `gorm:"type:float(8,2)" json:"price"`
	//Quantity   int      `gorm:"type:int(11)" json:"quantity"`
	//Category   Category `gorm:"foreignkey:CategoryID"`
}

func (u *Dishes) TableName() string {
	return "dishes"
}
