package models

type OrderItem struct {
	Model
	OrderID   uint    `json:"order_id"`
	DishesID  uint    `json:"dishes_id"`
	Price     float64 `json:"price"`
	Quantity  int64   `json:"quantity"`
	BasePrice float64 `json:"base_price"`
}

func (u *OrderItem) TableName() string {
	return "order_item"
}
