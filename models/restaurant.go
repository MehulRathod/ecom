package models

type Restaurant struct {
	Model
	Name    string `gorm:"type:varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci" json:"name"`
	Address string `gorm:"type:varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci" json:"address"`
	Mobile  string `gorm:"type:varchar(15)" sql:"index" json:"mobile" validate:"numeric"`
}

func (u *Restaurant) TableName() string {
	return "restaurants"
}
