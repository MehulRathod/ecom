package models

type Menu struct {
	Model
	RestaurantID uint       `type:"int(11)" sql:"index" json:"restaurant_id"`
	MenuName     string     `gorm:"type:varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci" sql:"index" json:"menu_name" validate:"required"`
	StatusId     string     `gorm:"type:enum('active','inactive','deleted')" sql:"index" json:"status_id"`
	Description  string     `gorm:"type:text" json:"description"`
	//Restaurant   Restaurant `gorm:"foreignkey:RestaurantID"`
}

func (u *Menu) TableName() string {
	return "menus"
}
