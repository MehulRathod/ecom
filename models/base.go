package models

import (
	"fmt"
	"github.com/go-redis/redis/v8"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	"os"
	"time"
)

var db *gorm.DB
var rdb *redis.Client

type Model struct {
	ID        uint       `gorm:"primary_key" json:"id,omitempty"`
	CreatedAt time.Time  `gorm:"not null" json:"created_at" sql:"DEFAULT:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time  `gorm:"not null" json:"updated_at" sql:"DEFAULT:CURRENT_TIMESTAMP"`
	DeletedAt *time.Time `sql:"index" json:"deleted_at,omitempty"`
}

type Test interface {
	value(val string)
}

func init() {

	e := godotenv.Load()
	if e != nil {
		fmt.Print(e)
	}
	rdb = redis.NewClient(&redis.Options{
		Addr: ":6379",
		DB:   6,
	})


	username := os.Getenv("db_user")
	password := os.Getenv("db_pass")
	dbName := os.Getenv("db_name")
	dbHost := os.Getenv("db_host")
	dbPort := os.Getenv("db_port")

	conn, err := gorm.Open("mysql", username+":"+password+"@tcp("+dbHost+":"+dbPort+")/"+dbName+"?charset=utf8mb4&parseTime=True&loc=Asia%2FKolkata")

	if err != nil {
		fmt.Print(err)
	}
	db = conn
	db.LogMode(true)

	db.Debug().AutoMigrate(
		&User{},
		&Category{},
		&Menu{},
		&Dishes{},
		&Order{},
		&Restaurant{},
		&OrderItem{},
	)

}

func GetDB() *gorm.DB {
	return db
}

func GetRedisDB() *redis.Client {
	return rdb
}
