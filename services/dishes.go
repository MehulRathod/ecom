package services

import (
	"fmt"
	u "testProject/apiHelpers"
	"testProject/models"
	"testProject/resources"
)

type DishesService struct {
	Dishes models.Dishes
}

func GetDishesByName(DishName string, CategoryID uint) (models.Dishes, error) {
	dishes := models.Dishes{}
	err := models.GetDB().Model(dishes).Select("*").Where("dish_name = ? AND category_id = ?", DishName, CategoryID).First(&dishes).Error
	if err != nil {
		return dishes, err
	}
	return dishes, nil
}

func (ds *DishesService) CreateDishes() map[string]interface{} {
	dishes := ds.Dishes

	res, err := GetDishesByName(dishes.DishName, dishes.CategoryID)
	if err == nil {
		fmt.Println(res)
		return u.Message(1, "Dish is already exists with this Category")
	}

	models.GetDB().Model(&dishes).Create(&dishes)
	dishesData := resources.DishesResponse{
		CategoryID: ds.Dishes.CategoryID,
		DishName:   ds.Dishes.DishName,
		Price:      ds.Dishes.Price,
	}

	response := u.Message(0, "Dish Created :)")
	response["data"] = dishesData
	return response
}
