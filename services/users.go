package services

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
	"log"
	"strconv"
	"strings"
	helpers "testProject/apiHelpers"
	u "testProject/apiHelpers"
	"testProject/models"
	"testProject/resources"
)

type UserService struct {
	User models.User
}

func (us *UserService) RegisterUser(ur resources.SignUpRequest) map[string]interface{} {
	user := us.User
	user.Name = ur.Name
	user.Email = ur.Email
	user.Image = ur.ImageName
	user.Password = ur.Password
	user.Mobile = ur.Mobile
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return u.Message(1, "Please try again")
	}
	user.Password = string(hashedPassword)

	res, err := GetUserByEmail(user.Email)
	if err == nil {
		fmt.Println("===", res)
		helpers.ImageDelete("images/users/" + user.Image)
		return u.Message(1, "Email is already registered with us.")
	}

	UserSignUp(user)
	userData := resources.UserSignUpResponse{
		Name:   user.Name,
		Email:  user.Email,
		Mobile: user.Mobile,
		Image:  "http://localhost:" + helpers.GetPort() + "/images/users/" + user.Image,
	}
	response := u.Message(0, "You have successfully signed up.")
	response["data"] = userData
	return response
}

func GetUserByEmail(email string) (models.User, error) {
	user := models.User{}
	err := models.GetDB().Model(user).Select("*").Where("email = ?", email).First(&user).Error
	if err != nil {
		return user, err
	}
	return user, nil
}

func UserSignUp(user models.User) {
	models.GetDB().Model(&user).Create(&user)
	return
}

func UserLogin(email_mobile, password string) map[string]interface{} {
	user := models.User{}

	validate := validator.New()
	passErrs := validate.Var(password, "required")
	if passErrs != nil {
		response := u.Message(1, "Password cannot be null")
		return response
	}

	checkMobileOrEmail := strings.Contains(email_mobile, "@")
	if checkMobileOrEmail == true {
		emailErrs := validate.Var(email_mobile, "required,email")

		if emailErrs != nil {
			for _, err := range emailErrs.(validator.ValidationErrors) {
				if err.ActualTag() == "required" {
					response := u.Message(1, "Email cannot be blank.")
					return response
				} else if err.ActualTag() == "email" {
					response := u.Message(1, "Please enter a valid email address.")
					return response
				}
			}
		}
		err := models.GetDB().Debug().Table("users").Where("email = ?", email_mobile).First(&user).Error
		if err != nil {
			if err == gorm.ErrRecordNotFound {
				return u.Message(1, "This email is not registered. Please try using another email address or Signup to create a new account.")
			}
			return u.Message(1, "Connection error. Please retry.")
		}

		err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
		if err != nil && err == bcrypt.ErrMismatchedHashAndPassword { //Password does not match!
			return u.Message(1, "Invalid login credentials. Please try again.")
		}

	} else {
		err := models.GetDB().Debug().Table("users").Where("mobile = ?", email_mobile).First(&user).Error

		if err != nil {
			if err == gorm.ErrRecordNotFound {
				return u.Message(1, "Mobile number not found.")
			}
			return u.Message(1, "Mobile Number is not valid. Please retry.")
		}
	}

	tokenData := &resources.PassDataToTokenUser{
		ID: user.ID,
	}

	var userData = tokenData
	token, _ := u.GenerateToken([]byte(u.SigningKey), userData)
	loginResponse := resources.LoginResponse{
		ID:     user.ID,
		Name:   user.Name,
		Email:  user.Email,
		Mobile: user.Mobile,
		Image:  user.Image,
		Token:  token,
	}
	fmt.Println("token ---> ", token)
	resp := u.Message(0, "Successfully Logged in")
	resp["data"] = loginResponse
	//resp["token"] = token
	return resp
}

//Router
func (us *UserService) CheckUser(id int) (s bool) {
	s = false
	type UserStatus struct {
		StatusId string
	}
	var res UserStatus
	result := models.GetDB().Table("users").Select("*").Where("id	=?", id).First(&res)

	if result.RowsAffected != 0 {
		s = true
	}
	return
}

func RegisteredUserList(c *gin.Context) map[string]interface{} {
	var v interface{}
	json.NewDecoder(c.Request.Body).Decode(&v)
	var UserList []resources.UserLoginResponse
	dbError := models.GetDB().Select("*").Table("users").Find(&UserList)
	if dbError.RowsAffected == 0 {
		return u.Message(0, "No Data found")
	}

	response := u.Message(0, "Registered User lists.")
	response["data"] = UserList
	return response
}

func (us *UserService) UserDetails(c *gin.Context) map[string]interface{} {
	user := c.MustGet("userData")
	data := user.(map[string]interface{})
	var ID = uint(data["ID"].(float64))

	log.Println(ID)
	strID := fmt.Sprintf("%v", ID)
	intID, _ := strconv.ParseInt(strID, 10, 64)

	us.User.ID = uint(intID)

	userData := models.GetDB().First(&us.User, intID)
	if userData.RowsAffected == 0 {
		u.Message(1, "User not found.")
	}

	UserResponse := resources.UserLoginResponse{
		ID:     us.User.ID,
		Name:   us.User.Name,
		Email:  us.User.Email,
		Mobile: us.User.Mobile,
		Image:  us.User.Image,
	}
	resp := u.Message(0, "User Details.")
	resp["data"] = UserResponse
	return resp
}

func GetUserById(id uint) (models.User, error) {
	user := models.User{}
	err := models.GetDB().Model(user).Select("name, email, image, mobile").Where("id = ?", id).First(&user).Error
	if err != nil {
		return user, err
	}
	return user, nil
}

func (us *UserService) UpdateDetails(ur resources.EditProfileRequest) map[string]interface{} {
	fmt.Println("service called")
	user := us.User
	user.Name = ur.Name
	user.ID = ur.ID
	user.Image = ur.ImageName
	user.Mobile = ur.Mobile

	res, err := GetUserById(user.ID)
	if err != nil {
		fmt.Println(res)
		helpers.ImageDelete("images/users/" + user.Image)
		return u.Message(1, "User not register with "+user.Email)
	}

	models.GetDB().Model(&user).Update(&user)
	helpers.ImageDelete("images/users/" + res.Image)

	userData := resources.UserSignUpResponse{
		Name:   user.Name,
		Email:  user.Email,
		Mobile: user.Mobile,
		Image:  "http://localhost:" + helpers.GetPort() + "/images/users/" + user.Image,
	}
	resp := u.Message(0, "User Details update successfully.")
	resp["data"] = userData
	return resp
}
