package services

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	u "testProject/apiHelpers"
	"testProject/models"
	"testProject/resources"
)

type CategoryService struct {
	Category models.Category
}

func GetCategoryByName(name string, id uint) (models.Category, error) {
	category := models.Category{}
	err := models.GetDB().Model(category).Select("*").Where("category_name = ?", name).Where("menu_id = ?", id).First(&category).Error
	if err != nil {
		return category, err
	}
	return category, nil
}

func (cs *CategoryService) CreateCategory(c *gin.Context) map[string]interface{} {
	category := cs.Category
	if category.StatusId == "" {
		category.StatusId = "active"
	} else {
		category.StatusId = cs.Category.StatusId
	}

	res, err := GetCategoryByName(category.CategoryName, category.MenuID)
	if err == nil {
		fmt.Println(res)
		return u.Message(1, "Category is already exists with this Menu")
	}

	models.GetDB().Model(&category).Create(&category)
	CategoryResponse := resources.CategoryResponse{
		MenuID:       cs.Category.MenuID,
		CategoryName: cs.Category.CategoryName,
		StatusId:     cs.Category.StatusId,
		Description:  cs.Category.Description,
	}

	StoreCategoriesInRedis(c)

	response := u.Message(0, "Category Created :)")
	response["data"] = CategoryResponse
	return response
}

func GetCategoryById(Id uint) (models.Category, error) {
	category := models.Category{}
	err := models.GetDB().Model(category).Select("*").Where("id = ?", Id).First(&category).Error
	if err != nil {
		return category, err
	}
	return category, nil
}

func (cs *CategoryService) EditCategory(c *gin.Context) map[string]interface{} {
	category := cs.Category

	res, err := GetCategoryById(category.ID)
	if err != nil {
		fmt.Println(res)
		return u.Message(1, "Enter valid category.")
	}

	models.GetDB().Model(&category).Update(&category)

	CategoryEditResponse := resources.CategoryEditResponse{
		MenuID:       cs.Category.MenuID,
		CategoryName: cs.Category.CategoryName,
		StatusId:     cs.Category.StatusId,
		Description:  cs.Category.Description,
	}

	StoreCategoriesInRedis(c)

	response := u.Message(0, "Successfully Updated Category Details :^)")
	response["data"] = CategoryEditResponse
	return response
}

func GetAllCategory(MenuID float64) []models.Category {
	var categoryList []models.Category
	models.GetDB().Table("category").Select("*").
		Where("category.status_id = ? AND category.menu_id = ?", "active", MenuID).First(&categoryList)

	return categoryList
}

func GetAllCategoryList(c *gin.Context) []models.Category {
	var v interface{}
	err := json.NewDecoder(c.Request.Body).Decode(&v)
	if err != nil {
		u.Respond(c.Writer, u.Message(1, "Invalid request"))
	}
	m := v.(map[string]interface{})
	MenuID := m["menu_id"].(float64)
	userData := GetAllCategory(MenuID)

	return userData
}

func StoreCategoriesInRedis(c *gin.Context) {
	categories, _ := json.Marshal(GetAllCategoryList(c))
	models.GetRedisDB().Set(c, u.CATEGORIES, string(categories), 0)
}
