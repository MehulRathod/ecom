package services

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	u "testProject/apiHelpers"
	"testProject/models"
	"testProject/resources"
)

type MenuService struct {
	Menu models.Menu
}

/* 							------->			Create Menu			<---------							*/

func GetMenuByName(name string) (models.Menu, error) {
	menu := models.Menu{}
	err := models.GetDB().Model(menu).Select("*").Where("name = ?", name).First(&menu).Error
	if err != nil {
		return menu, err
	}
	return menu, nil
}

func (ms *MenuService) CreateMenu(c *gin.Context) map[string]interface{} {
	menu := ms.Menu
	if menu.StatusId == "" {
		menu.StatusId = "active"
	} else {
		menu.StatusId = ms.Menu.StatusId
	}

	res, err := GetMenuByName(menu.MenuName)
	if err == nil {
		fmt.Println(res)
		return u.Message(1, "Menu is already exists Please create another one. :/)")
	}

	models.GetDB().Model(&menu).Create(&menu)
	MenuResponse := resources.MenuResponse{
		RestaurantID: ms.Menu.RestaurantID,
		MenuName:    ms.Menu.MenuName,
		StatusId:    ms.Menu.StatusId,
		Description: ms.Menu.Description,
	}

	StoreMenuInRedis(c)

	response := u.Message(0, "Menu Created :)")
	response["data"] = MenuResponse
	return response
}

/* 							------->			Edit Menu			<---------							*/

func GetMenuById(Id uint, RestaurantID uint) (models.Menu, error) {
	menu := models.Menu{}
	err := models.GetDB().Model(menu).Select("*").Where("id = ? AND restaurant_id", Id, RestaurantID).First(&menu).Error
	if err != nil {
		return menu, err
	}
	return menu, nil
}

func (ms *MenuService) EditMenu(c *gin.Context) map[string]interface{} {
	menu := ms.Menu

	res, err := GetMenuById(menu.ID, menu.RestaurantID)
	if err != nil {
		fmt.Println(res)
		return u.Message(1, "Enter valid menu.")
	}

	models.GetDB().Model(&menu).Update(&menu)

	MenuEditResponse := resources.MenuEditResponse{
		RestaurantID: ms.Menu.RestaurantID,
		MenuName:    ms.Menu.MenuName,
		StatusId:    ms.Menu.StatusId,
		Description: ms.Menu.Description,
	}

	StoreMenuInRedis(c)

	response := u.Message(0, "Successfully Updated Menu Details :^)")
	response["data"] = MenuEditResponse
	return response
}

/* 							------->			Get All Menu			<---------							*/

func  GetAllMenu(RestaurantID float64) []models.Menu {
	var menuList []models.Menu
	models.GetDB().Debug().Select("*").Table("menus").
		Where("menus.status_id = ? AND menus.restaurant_id = ?", "active", RestaurantID).Find(&menuList)
	fmt.Println("menus", menuList)
	return menuList
}

func GetMenuList(c *gin.Context) []models.Menu {
	var v interface{}
	err := json.NewDecoder(c.Request.Body).Decode(&v)
	if err != nil {
		u.Respond(c.Writer, u.Message(1, "Invalid request"))
	}
	m := v.(map[string]interface{})
	RestaurantID := m["restaurant_id"].(float64)

	menuData := GetAllMenu(RestaurantID)
	fmt.Println("menuData >>>", menuData)
	return menuData
}

func StoreMenuInRedis(c *gin.Context) {
	menus, _ := json.Marshal(GetMenuList(c))
	models.GetRedisDB().Set(c, u.MENU, string(menus), 0)
}
