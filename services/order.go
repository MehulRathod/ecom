package services

import (
	"fmt"
	"testProject/models"
)

type OrderService struct {
	OrderItem models.OrderItem
}

func GetOrderItemByID(ID uint) (models.OrderItem, error) {
	orderItem := models.OrderItem{}
	err := models.GetDB().Model(orderItem).Select("*").Where("id = ?", ID).First(&orderItem).Error
	if err != nil {
		return orderItem, err
	}
	return orderItem, nil
}

/*func CalculateBasePrice(Price float64, Quantity int64) ([]models.OrderItem, error) {
	basePrice := []models.OrderItem{}
	err := models.GetDB().Model(basePrice).Select("price * quantity").Where("price = ? AND quantity = ?",Price, Quantity).First(&basePrice).Error
	if err != nil {
		return basePrice, err
	}
	return basePrice, nil
}
*/
func (os *OrderService) CreateOrder(restaurantId float64, ID uint) float64 {
	var orderId float64
	models.GetDB().Debug().Exec("INSERT INTO orders (restaurant_id, user_id) values (?, ?)", restaurantId, ID).Last(&orderId)
	fmt.Println(orderId)
	return orderId
}
func (os *OrderService) AddItemToOrder(orderId float64, itemId float64, qty float64) {
	//orderItem := os.OrderItem

	models.GetDB().Debug().Exec("INSERT INTO order_item (order_id, dishes_id, price, quantity, base_price) values (?, ?, (select dishes.price from dishes where dishes.id = ?), ?, (select dishes.price*" + fmt.Sprintf("%.6f", qty) + " as base_total from dishes where dishes.id = ?))", orderId, itemId, itemId, qty, itemId)

	//models.GetDB().Model(&orderItem).
	return
}
/*func (os *OrderService) CreateOrder(c *gin.Context) map[string]interface{} {
	userInfo := c.MustGet("userData")
	data := userInfo.(map[string]interface{})
	var ID = uint(data["ID"].(float64))
	var v interface{}
	json.NewDecoder(c.Request.Body).Decode(&v)
	pa := v.(map[string]interface{})

	tx := models.GetDB().Begin()

	var categoryResponse resources.CategoryResponse

	var menuId = pa["menu_id"].(string)
	if menuId != "" {
		dbErrMenuId := models.GetDB().Table("category").Select("category.*, menus.id").
			Joins("left join menus on menus.id = category.menu_id").Where("category.status_id = ? AND menus.id", "active", menuId).
			Find(&categoryResponse)
		if dbErrMenuId.RowsAffected == 0{
			tx.Rollback()
			return u.Message(1, "No Menu found")
		}
	}

	//Create Order
	order := models.Order{}
	order.UserID = ID
	order.OrderPrice = 0.0
	order.StatusId = "active"

	if result := tx.Create(&order); result.Error != nil {
		tx.Rollback()
		return u.Message(1, "Failed to create order")
	}


	// total order amount calculation
	userData := resources.OrderResponse{
		RestaurantID:  os.Order.RestaurantID,
		UserID:        os.Order.UserID,
		OrderPrice:    os.Order.OrderPrice,
	}
	response := u.Message(0, "Order added successfully.")
	response["data"] = userData
	return response
}
*/