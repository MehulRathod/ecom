package services

import (
	"fmt"
	u "testProject/apiHelpers"
	"testProject/models"
	"testProject/resources"
)

type RestaurantService struct {
	Restaurant models.Restaurant
}

func GetRestaurantByName(name string) (models.Restaurant, error) {
	restaurant := models.Restaurant{}
	err := models.GetDB().Model(restaurant).Select("*").Where("name = ?", name).First(&restaurant).Error
	if err != nil {
		return restaurant, err
	}
	return restaurant, nil
}

func (rs *RestaurantService) CreateRestaurant() map[string]interface{} {
	restaurant := rs.Restaurant

	res, err := GetRestaurantByName(restaurant.Name)
	if err == nil {
		fmt.Println(res)
		return u.Message(1, "Menu is already exists Please create another one. :/)")
	}

	models.GetDB().Model(&res).Create(&restaurant)
	RestaurantResponse := resources.RestaurantResponse{
		RestaurantID: rs.Restaurant.ID,
		Name:         rs.Restaurant.Name,
		Address:      rs.Restaurant.Address,
		Mobile:       rs.Restaurant.Mobile,
	}

	response := u.Message(0, "Restaurant Created :)")
	response["data"] = RestaurantResponse
	return response
}
