package apiHelpers

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"
)

const (
	SigningKey = "AK8}<|Vw4>F&y!I8.O>&B}F(gd4N[i"
	//SigningKey = "abc123456789"
	CATEGORIES = "CATEGORIES"
	MENU       = "MENU"
)

func ImageDelete(path string) {
	os.Remove(path)
}

func ImageUpload(c *gin.Context, dirName string, prefix string) (string, bool) {
	file, handler, err := c.Request.FormFile("image")
	if err != nil {
		fmt.Println(err)
		return "", false
	}
	defer file.Close()

	// to manage maximum size
	if handler.Size > 3690254 {
		return "", true
	}

	extension := filepath.Ext(handler.Filename)

	// assign image(path) to temp variable with prefix product name
	tempFile, err := ioutil.TempFile(dirName, prefix+"-*"+extension)
	if err != nil {
		fmt.Println(err)
	}
	defer tempFile.Close()

	// read image
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}
	// write image
	tempFile.Write(fileBytes)
	fmt.Println("tempFile >>>", tempFile)

	//if len(strings.Split(tempFile.Name(), "/")) > 0 {
	//	return strings.Split(tempFile.Name(), "/")[4], false
	//}
	return tempFile.Name(), false
}

func GetPort() string {
	port := os.Getenv("port")
	if port == "" {
		port = "8080" //localhost
	}
	return port
}

func AuthUser(c *gin.Context) map[string]interface{} {
	userId := c.MustGet("userData")
	data := userId.(map[string]interface{})
	return data
}

func GenerateToken(k []byte, userData interface{}) (string, error) {
	// Create the token
	token := jwt.New(jwt.SigningMethodHS256)
	// Set some claims
	claims := make(jwt.MapClaims)
	claims["userData"] = userData
	claims["exp"] = time.Now().Add(time.Hour * 8760).Unix()
	token.Claims = claims
	// Sign and get the complete encoded token as a string
	tokenString, err := token.SignedString(k)
	return tokenString, err
}

func ValidateToken(t string, k string) (*jwt.Token, error) {
	token, err := jwt.Parse(t, func(token *jwt.Token) (interface{}, error) {
		return []byte(k), nil
	})

	return token, err
}
