package routers

import (
	"github.com/gin-gonic/gin"
	contollers "testProject/controllers"
	"testProject/middlewares"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()

	r.Use(func(c *gin.Context) {
		// add header Access-Control-Allow-Origin
		c.Writer.Header().Set("Content-Type", "application/json")
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Max-Age", "86400")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, UPDATE")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, X-Max")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(200)
		} else {
			c.Next()
		}
	})
	r.Static("/static", "static")
	r.Static("/templates", "templates")
	r.LoadHTMLGlob("templates/*")

	route := r.Group("/api")
	route.POST("/register", contollers.Register)
	route.POST("/login", contollers.Login)

	route.POST("/restaurant", contollers.Restaurant)

	route.GET("list-users", contollers.ListOfUsers)
	route.Use(middlewares.AuthHandlerAPI())
	{
		route.GET("user-detail", contollers.UserDetails)
		route.PUT("update-details", contollers.UpdateUserDetails)

		//	menu
		route.POST("create-menu", contollers.CreateMenu)
		route.PUT("edit-menu", contollers.EditMenu)
		route.GET("list-menu", contollers.GetAllMenu)

		//	category
		route.POST("create-category", contollers.CreateCategory)
		route.PUT("edit-category", contollers.EditCategory)
		route.GET("list-category", contollers.GetAllCategory)

		// dishes
		route.POST("create-dishes", contollers.CreateDishes)
		route.POST("create-order", contollers.CreateOrder)
	}

	return r
}
